from collections import deque

INFIN = 100000

def weight(ui, vi, ei):
    for e in ei:
        if ui == e[0] and vi == e[1]:
            return e[2]
        elif ui == e[1] and vi == e[0]:
            return e[2]
    return -1

def adj(vi, ei):
    sn = []
    for e in ei:
        if vi == e[0]:
            sn.append(e[1])
        elif vi == e[1]:
            sn.append(e[0])

    return sn;

def Prim(E,V, s = 0):
    m = len(E)
    n = len(V)
    S = []
    P = [-1] * n
    D = [INFIN] * n

    D[s] = 0

    Q = list(V)

    while len(Q) > 0:
        Q = deque(sorted(Q, key=lambda node: D[node]))
        v = Q.popleft()
        for u in adj(v,E):
            if Q.count(u) and D[u] > weight(u,v,E):
                P[u] = v
                D[u] = weight(u,v,E)

    return P

gv = list(range(0,8))
ge = [(0,2,1),(2,3,3),(3,7,3),(7,6,6),(6,4,4),(4,1,6),(1,5,2),(5,0,5),(0,1,3),(2,1,4),(3,1,4)]

gr = Prim(ge,gv)

print(gr)


