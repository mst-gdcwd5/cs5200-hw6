import numpy

NE = numpy.inf


# FloydWorshall
# input, adj matrix
def FloydWorshall(W):
    D = [W]
    n = len(W)
    P = numpy.zeros(shape=(n, n))

    for i in range(n):
        for j in range(n):
            if i == j:
                P[i][j] = NE
            elif W[i][j] == NE:
                P[i][j] = NE
            else:
                P[i][j] = i + 1

    # for all possible intermediate nodes
    for k in range(1,n+1):
        D.append(numpy.zeros(shape=(n,n)))
        # for all cells
        for i in range(0,n):
            for j in range(0,n):
                # if new weight cheaper, update, else use same value
                if D[k-1][i][j] <= (D[k-1][i][k-1] + D[k-1][k-1][j]):
                    D[k][i][j] = D[k-1][i][j]
                else:
                    D[k][i][j] = D[k-1][i][k-1] + D[k-1][k-1][j]
                    P[i][j] = P[k-1][j]
    return D[k],P

# TransitiveClousure Algorithm, based on FloydWorshall
# input, connected matrix
def TransitiveClosure(W):
    D = [W]
    n = len(W)

    # for all possible intermediate nodes
    for k in range(1,n+1):
        # copy previous iteration
        D.append(list(D[k-1]))
        # for all cells
        for i in range(0,n):
            for j in range(0,n):
                # if connected by intermediate node, update
                if D[k-1][i][k-1] and D[k-1][k-1][j]:
                    D[k][i][j] = 1
    return D[k]

# convert weight to connected matrix
# input, weight adjacency matrix
def WeightToConnect(W):
    n = len(W)

    for i in range (0,n):
        for j in range (0,n):
            if W[i][j] == NE:
                W[i][j] = 0
            elif W[i][j] != 0:
                W[i][j] = 1
    return W


gw = [[0 ,3 ,8 ,NE,-4],
      [NE,0 ,NE,1 ,7 ],
      [NE,4 ,0 ,NE,NE],
      [2 ,NE,-5,0 ,NE],
      [NE,NE,NE,6 ,0 ]]

print("Graph 1")
print(gw)

fwr = FloydWorshall(gw)

print("FW-D")
print(fwr[0])
print("FW-P")
print(fwr[1])

gw = WeightToConnect(gw)

tcr = TransitiveClosure(gw)

print("TC")
print(tcr)

gn = [[0 ,1 ,0,99],
      [NE,0 ,1 ,NE],
      [NE,NE,0 ,NE],
      [NE,5 ,NE,0 ]
      ]

print("Graph 2")

print(gn)

fwr = FloydWorshall(gn)

print("FW-D")
print(fwr[0])
print("FW-P")
print(fwr[1])

gw = WeightToConnect(gn)

tcr = TransitiveClosure(gn)
print("TC")
print(tcr)

print("Graph 3")
print(gw)

gw = [[0 ,0 ,1 ,0 ],
      [1 ,0 ,0 ,1 ],
      [0 ,0 ,0 ,0 ],
      [0 ,1 ,0 ,0 ]]

print("TC")
tcr = TransitiveClosure(gw)

print(tcr)