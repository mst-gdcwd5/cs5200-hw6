from collections import deque
import numpy as np
import time

INFIN = np.inf

# return weight of edge between nodes ui,vi
# return -1 if no edge exists
def weight(ui, vi, ei):
    for e in ei:
        if ui == e[0] and vi == e[1]:
            return e[2]
        elif ui == e[1] and vi == e[0]:
            return e[2]
    return -1

# return list of nodes adjacent to node v
# given list of edge ei
# returns [] if no adj nodes
def adj(vi, ei):
    sn = []
    for e in ei:
        if vi == e[0]:
            sn.append(e[1])
        elif vi == e[1]:
            sn.append(e[0])

    return sn;

# Prim's Algorithm
# Input, List of Edges, Vertices, Index of Start Node
def Prim(E,V, s = 0):
    #ititalize
    m = len(E)
    n = len(V)
    S = []
    P = [-1] * n # parent array
    D = [INFIN] * n # distance array

    #distance of source node
    D[s] = 0

    #copy vertices into set of unvisited
    Q = list(V)

    while len(Q) > 0:
        # get minimum distance node
        Q = deque(sorted(Q, key=lambda node: D[node]))
        v = Q.popleft()
        # for all adj nodes
        for u in adj(v,E):
            #update weight value & parent if lower
            if Q.count(u) and D[u] > weight(u,v,E):
                P[u] = v
                D[u] = weight(u,v,E)

    #Return Parent & Distance Array
    return P, D

def Kruskal(E,V):
    S = []
    #sort edges by weight
    E = sorted(E, key=lambda edge: edge[2])
    m = len(E)
    n = len(V)
    # array of connected component indicators for nodes
    CC = [0] * n

    # init array of connected component ids to all seperate components
    for i in range(0,n):
        CC[i] = i

    # for all edges, ordered by weight
    for j in range(0,m):
        u = E[j][0]
        v = E[j][1]
        # if seperate components
        if CC[u] != CC[v]:
            # add edge to set
            S.append((u,v))
            # update connected component
            cID = CC[v]
            for p in V:
                if CC[p] == cID:
                    CC[p] = CC[u]
    # return list of edges selected
    return S

gv = list(range(0,8))
ge = [(0,2,1),(2,3,3),(3,7,3),(7,6,6),(6,4,4),(4,1,6),(1,5,2),(5,0,5),(0,1,3),(2,1,4),(3,1,4)]

print("Graph 1")
print(ge)
print("P1")
p1s = time.time()
for x in range(100):
    gr = Prim(ge,gv)
p1f = time.time()

p1 = p1f - p1s

print(p1*1000)

print(gr)
print("K1")
k1s = time.time()
for x in range(100):
    gr = Kruskal(ge,gv)
k1f = time.time()

k1 = k1f - k1s
print(k1*1000)

print(gr)

gv = list(range(0,8))
ge = [(0,1,1),(1,5,1),(5,7,6),(7,4,4),(4,3,2),(3,2,4),(2,0,2),(1,2,4),(2,5,5),(2,4,3),(3,6,3)]

print("Graph 2")
print(ge)
print("P2")

p2s = time.time()
for x in range(100):
    gr = Prim(ge,gv)
p2f = time.time()

p2 = p2f - p2s

print(p2*1000)

print(gr)

print("K2")
k2s = time.time()
for x in range(100):
    gr = Kruskal(ge,gv)
k2f = time.time()

k2 = k2f - k2s

print(k2*1000)

print(gr)


