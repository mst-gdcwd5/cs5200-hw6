import numpy as np
import heapq

NE = np.inf

# weight of edge in directed graph
# input, vertex indexes, set of edges
def weight(ui, vi, ei):
    return ei[ui][vi]


# list of adj nodes in directed graph
# input, source node, list of edges
def adj(vi, ei):
    psl = ei[vi]
    sn = []

    # for all entries in row of adj matrix
    for x in range(len(ei)):
        if psl[x] != NE:
            sn.append(x)

    # return list of nodes
    return sn;


def Dijkstra(E,V, s = 0):
    m = len(E)
    n = len(V)

    P = [-1] * n # parent array
    D = [np.inf] * n # distance array
    Vs = [0] * n # visited indicator array



    D[s] = 0
    P[s] = -1
    Vs[s] = 1
    k = 0

    PQ = []

    # init
    # start values of distance, parents
    # build priority queue
    for q in V:
        if q != s:
            Vs[q] = 0
            if adj(s,E).count(q):
                D[q] = weight(s,q,E)
                P[q] = s
            else:
                D[q] = np.inf
                P[q] = np.inf
        heapq.heappush(PQ, (D[q],q))

    # while nodes to select
    while k < n:
        # select least distance node
        minV = heapq.heappop(PQ)[1]
        Vs[minV] = 1
        k += 1

        # for all adjancent nodes
        for q in adj(minV, E):
            # update weight if lower
            if Vs[q] == 0 and D[q] > (D[minV] + weight(minV,q, E)):
                D[q] = D[minV] + weight(minV,q, E)
                P[q] = minV
    #return distance array, parent array, and visited array
    return D, P, Vs

gv = list(range(0,8))
ge = [[0 ,1 ,2 ,NE,NE,NE,NE,NE],
      [1 ,0 ,4 ,NE,NE,1 ,NE,NE],
      [2 ,4 ,0 ,4 ,3 ,5 ,NE,NE],
      [NE,NE,4 ,0 ,2 ,NE,3 ,NE],
      [NE,NE,3 ,2 ,0 ,NE,NE,4 ],
      [NE,1 ,5 ,NE,NE,0 ,NE,6 ],
      [NE,NE,NE,3 ,NE,NE,0 ,NE],
      [NE,NE,NE,NE,4 ,6 ,NE,0 ]
      ]

dr = Dijkstra(ge,gv)

print("Dijkstra Example")
for x in ge:
    print(x)
print("\n")
print("MinDist")
print(dr[0])
print("Parent")
print(dr[1])

gnv = list(range(0,4))
gn = [[0 ,1 ,0,99],
      [NE,0 ,1 ,NE],
      [NE,NE,0 ,NE],
      [NE,-300,NE,0 ]
      ]

dr = Dijkstra(gn,gnv)

print("\n")
print("Dijkstra Fail Example")
for x in gn:
    print(x)
print("\n")
print("MinDist")
print(dr[0])
print("Parent")
print(dr[1])