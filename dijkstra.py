import numpy

INFIN = 100000
NE = INFIN

def weight(ui, vi, ei):
    for e in ei:
        if ui == e[0] and vi == e[1]:
            return e[2]
        elif ui == e[1] and vi == e[0]:
            return e[2]
    return -1

def adj(vi, ei):
    sn = []
    for e in ei:
        if vi == e[0]:
            sn.append(e[1])
        elif vi == e[1]:
            sn.append(e[0])

    return sn;

def Dijkstra(E,V, s = 0):
    m = len(E)
    n = len(V)

    P = [-1] * n
    D = [INFIN] * n
    Vs = [0] * n



    D[s] = 0
    P[s] = -1
    Vs[s] = 1
    k = 0

    for q in V:
        if q != s:
            Vs[q] = 0
            if adj(s,E).count(q):
                D[q] = weight(s,q,E)
                P[q] = s
            else:
                D[q] = INFIN
                P[q] = -1

    while k < n:
        minD = INFIN
        minV = -1
        for i in range(0,n):
            if Vs[i] == 0 and D[i] < minD:
                minD = D[i]
                minV = i
        Vs[minV] = 1
        k += 1

        for q in adj(minV, E):
            if Vs[q] == 0 and D[q] > (D[minV] + weight(minV,q, E)):
                D[q] = D[minV] + weight(minV,q, E)
                P[q] = minV
    return D, P, Vs

gv = list(range(0,8))
ge = [(0,1,1),(1,5,1),(5,7,6),(7,4,4),(4,3,2),(3,2,4),(2,0,2),(1,2,4),(2,5,5),(2,4,3),(3,6,3)]

dr = Dijkstra(ge,gv)

print(dr[0])
print(dr[1])

